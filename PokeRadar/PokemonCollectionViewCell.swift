//
//  PokemonCollectionViewCell.swift
//  PokeRadar
//
//  Created by Moisés Córdova on 2/20/18.
//  Copyright © 2018 Moisés Córdova. All rights reserved.
//

import UIKit

class PokemonCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var pokemonImageView: UIImageView!
    @IBOutlet weak var pokemonIDLabel: UILabel!
    @IBOutlet weak var pokemonLabel: UILabel!
    
}
